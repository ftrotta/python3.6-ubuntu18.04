# `tensorflow` and `tensorflow-gpu`

The pip package `tensorflow` for versions following 1.15
provides support for NVIDIA GPU as well. In other words,
there is no need to explicitly `pip install tensorflow-gpu`.
This greatly simplifies the management of the tensorflow dependency
in Python.

However, official `tensorflow/tensorflow:2.4.2-gpu` Docker container
includes `tensorflow-gpu`, while `tensorflow/tensorflow:2.4.2`
includes `tensorflow`. This does not happen in official 
`tensorflow/tensorflow:2.5.0` and `tensorflow/tensorflow:2.5.0-gpu` Docker containers,
that do not differ in the `tensorflow` pip package.

Containers of this folder include NVIDIA drivers and the
`tensorflow` (simple, non-GPU) package. In other words
they follow the convention of the latest official Tensorflow
containers.