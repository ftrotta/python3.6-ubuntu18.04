# Jupyter with Tensorflow 2.4.0 and NVIDIA GPU support

It includes
[Jupyter notebook extensions](https://github.com/ipython-contrib/jupyter_contrib_nbextensions).

To run:

```bash
docker run \
    --rm \
    -it \
    --gpus all \
    -p 8888:8888 \
    -p 6006:6006 \
    -v $(pwd):/tf/notebooks \
    -u $(id -u):$(id -g) \
    registry.gitlab.com/ftrotta/python3.6-ubuntu18.04:tensorflow2.4.0-gpu-jupyter-01
```

Use `ftrotta` as password at login.

The image can also be used on hosts lacking gpus: just remove the ``--gpus`` switch.

## Tensorboard log dir

Tensorboard is started automatically.

The log dir is `/tf/notebooks/tb_logs`. It can be modified by setting the ``TENSORBOARD_LOGDIR``
environment variable.