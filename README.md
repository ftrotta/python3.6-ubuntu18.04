# Python 3.6 on Ubuntu 18.04

Docker images to create running/development environments for Python3.x software
on Ubuntu 18.04.

This version of Ubuntu is the only currently supported by
[Nvidia CUDA in Docker](https://hub.docker.com/r/nvidia/cuda/), that are the base
for [Tensorflow GPU images](https://hub.docker.com/r/tensorflow/tensorflow).

The images are published to the Gitlab Container Registry of this repository, 
`registry.gitlab.com/ftrotta/python3.6-ubuntu18.04`. Folders correspond to tags.

## Why not the latest Python version?

Ubuntu18.04 Docker image comes with no Python installed. The version of Python 
installed via `apt install python3-pip` is 3.6.

## Date and time

Containers based on these image will have the Europe/Rome date and time. 